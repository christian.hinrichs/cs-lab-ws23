{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "9d4fc489-5996-4f32-a42d-f5a936488105",
   "metadata": {},
   "source": [
    "# Getting familiar with OpenSSL\n",
    "\n",
    "[OpenSSL](https://www.openssl.org/docs/man3.0/man1/) is a cryptography toolkit implementing the Secure Sockets Layer (SSL v2/v3) and Transport Layer Security (TLS v1) network protocols and related cryptography standards required by them.\n",
    "\n",
    "The `openssl` program is a command line program for using the various cryptography functions of OpenSSL's `crypto` library from the shell. It can be used for\n",
    "\n",
    "*  Creation and management of private keys, public keys and parameters\n",
    "*  Public key cryptographic operations\n",
    "*  Creation of X.509 certificates, CSRs and CRLs\n",
    "*  Calculation of Message Digests and Message Authentication Codes\n",
    "*  Encryption and Decryption with Ciphers\n",
    "*  SSL/TLS Client and Server Tests\n",
    "*  Handling of S/MIME signed or encrypted mail\n",
    "*  Timestamp requests, generation and verification\n",
    "\n",
    "The `openssl` program provides a rich variety of commands. `openssl help` will print the available commands. Each command can have many options and argument parameters. Detailed documentation and use cases for most standard openssl commands are available (e.g., [openssl-dhparam](https://www.openssl.org/docs/man3.0/man1/openssl-dhparam.html) or [openssl-genpkey](https://www.openssl.org/docs/man3.0/man1/openssl-genpkey.html))."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "741e15ec-cdd3-4e73-8702-1ae4bb157c89",
   "metadata": {},
   "source": [
    "*Remember to put an exclamation mark `!` before a shell command in Jupyter Notebooks. This instructs the kernel to run this through the shell instead of the Python interpreter.*  \n",
    "*Also, you may use Python variables within shell commands by using curly braces `{` `}`.*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "c06470f5-93a3-41e5-b28c-5a1ad11eb024",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Standard commands\n",
      "asn1parse         ca                ciphers           cms               \n",
      "crl               crl2pkcs7         dgst              dhparam           \n",
      "dsa               dsaparam          ec                ecparam           \n",
      "enc               engine            errstr            gendsa            \n",
      "genpkey           genrsa            help              list              \n",
      "nseq              ocsp              passwd            pkcs12            \n",
      "pkcs7             pkcs8             pkey              pkeyparam         \n",
      "pkeyutl           prime             rand              rehash            \n",
      "req               rsa               rsautl            s_client          \n",
      "s_server          s_time            sess_id           smime             \n",
      "speed             spkac             srp               storeutl          \n",
      "ts                verify            version           x509              \n",
      "\n",
      "Message Digest commands (see the `dgst' command for more details)\n",
      "blake2b512        blake2s256        gost              md4               \n",
      "md5               mdc2              rmd160            sha1              \n",
      "sha224            sha256            sha3-224          sha3-256          \n",
      "sha3-384          sha3-512          sha384            sha512            \n",
      "sha512-224        sha512-256        shake128          shake256          \n",
      "sm3               \n",
      "\n",
      "Cipher commands (see the `enc' command for more details)\n",
      "aes-128-cbc       aes-128-ecb       aes-192-cbc       aes-192-ecb       \n",
      "aes-256-cbc       aes-256-ecb       aria-128-cbc      aria-128-cfb      \n",
      "aria-128-cfb1     aria-128-cfb8     aria-128-ctr      aria-128-ecb      \n",
      "aria-128-ofb      aria-192-cbc      aria-192-cfb      aria-192-cfb1     \n",
      "aria-192-cfb8     aria-192-ctr      aria-192-ecb      aria-192-ofb      \n",
      "aria-256-cbc      aria-256-cfb      aria-256-cfb1     aria-256-cfb8     \n",
      "aria-256-ctr      aria-256-ecb      aria-256-ofb      base64            \n",
      "bf                bf-cbc            bf-cfb            bf-ecb            \n",
      "bf-ofb            camellia-128-cbc  camellia-128-ecb  camellia-192-cbc  \n",
      "camellia-192-ecb  camellia-256-cbc  camellia-256-ecb  cast              \n",
      "cast-cbc          cast5-cbc         cast5-cfb         cast5-ecb         \n",
      "cast5-ofb         des               des-cbc           des-cfb           \n",
      "des-ecb           des-ede           des-ede-cbc       des-ede-cfb       \n",
      "des-ede-ofb       des-ede3          des-ede3-cbc      des-ede3-cfb      \n",
      "des-ede3-ofb      des-ofb           des3              desx              \n",
      "idea              idea-cbc          idea-cfb          idea-ecb          \n",
      "idea-ofb          rc2               rc2-40-cbc        rc2-64-cbc        \n",
      "rc2-cbc           rc2-cfb           rc2-ecb           rc2-ofb           \n",
      "rc4               rc4-40            seed              seed-cbc          \n",
      "seed-cfb          seed-ecb          seed-ofb          sm4-cbc           \n",
      "sm4-cfb           sm4-ctr           sm4-ecb           sm4-ofb           \n",
      "\n"
     ]
    }
   ],
   "source": [
    "!openssl help"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7274f92a-b5ff-47b4-be02-bfb671872988",
   "metadata": {},
   "source": [
    "As a first step in this exercise, we will have a look at the `s_client` command. The [documentation](https://www.openssl.org/docs/man3.0/man1/openssl-s_client.html) says:\n",
    "\n",
    "> This command implements a generic SSL/TLS client which connects to a remote host using SSL/TLS. It is a *very* useful diagnostic tool for SSL servers."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fc7376e7-8c45-4673-b322-9b84b8090309",
   "metadata": {},
   "source": [
    "Let's define the host we want to connect to and a request that will be sent to the host:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "42981974-9109-4ffb-8c64-f4dddd4c6474",
   "metadata": {},
   "outputs": [],
   "source": [
    "host = 'www.jade-hs.de'\n",
    "request = f'HEAD / HTTP/1.0\\nHost: {host}\\n'"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1ba74fbc-a5dd-48aa-932f-7ced25f7884f",
   "metadata": {},
   "source": [
    "When typed into an established connection, the request would look like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "3d2ec05a-efd4-48fc-aeb6-c9b6d3534752",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "HEAD / HTTP/1.0\n",
      "Host: www.jade-hs.de\n",
      "\n"
     ]
    }
   ],
   "source": [
    "!echo -e \"{request}\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f6e1caba-42ee-4f46-aa88-c9e1f7816667",
   "metadata": {},
   "source": [
    "This means we are requesting header information from the `/` URI at the host `www.jade-hs.de` using the `HTTP` protocol, version `1.0`.\n",
    "\n",
    "More specifically (see [RFC9110](https://www.rfc-editor.org/rfc/rfc9110.html#name-methods)), the HTTP `HEAD` method will instruct the host to send HTTP headers just as if the document was requested using the HTTP GET method. The only difference between HTTP HEAD and GET requests is that for HTTP HEAD, the server only returns headers without body. It is often used to retrieve meta-information about a resource at a specified URI without transmitting actual data. For example, to check the availability of hypertext links (check for broken links) or to read the *Content-Length* header of a download to check the filesize without actually downloading the file.\n",
    "\n",
    "In our case, we will use this as the most basic request just to be able to examine the SSL/TLS connection parameters."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4215dc72-7d85-4d47-9164-d299c608a33f",
   "metadata": {},
   "source": [
    "The following line will establish a SSL/TLS connection to the host via the `s_client` command and print out diagnostic information about the connection. Normally, this is an interactive command and you would be able to speak with the webserver by typing in commands directly (i.e., the connection is established and then you would have to issue a request to the remote webserver). But in order to automate it in this notebook, we configure the `s_client` with the `-crlf` and `-ign_eof` options so that it can interpret piped data correctly and then we pipe our request into the connection. After a short timeout (some seconds), the remote host then closes the connection afterwards."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "c82c5168-ca01-4aaa-af2a-204c1c9cd9b5",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "CONNECTED(00000003)\n",
      "depth=2 C = US, ST = New Jersey, L = Jersey City, O = The USERTRUST Network, CN = USERTrust RSA Certification Authority\n",
      "verify return:1\n",
      "depth=1 C = GB, ST = Greater Manchester, L = Salford, O = Sectigo Limited, CN = Sectigo RSA Organization Validation Secure Server CA\n",
      "verify return:1\n",
      "depth=0 C = DE, ST = Niedersachsen, O = Jade Hochschule Wilhelmshaven / Oldenburg / Elsfleth, CN = www.jade-hs.de\n",
      "verify return:1\n",
      "---\n",
      "Certificate chain\n",
      " 0 s:C = DE, ST = Niedersachsen, O = Jade Hochschule Wilhelmshaven / Oldenburg / Elsfleth, CN = www.jade-hs.de\n",
      "   i:C = GB, ST = Greater Manchester, L = Salford, O = Sectigo Limited, CN = Sectigo RSA Organization Validation Secure Server CA\n",
      " 1 s:C = DE, ST = Niedersachsen, O = Jade Hochschule Wilhelmshaven / Oldenburg / Elsfleth, CN = www.jade-hs.de\n",
      "   i:C = GB, ST = Greater Manchester, L = Salford, O = Sectigo Limited, CN = Sectigo RSA Organization Validation Secure Server CA\n",
      " 2 s:C = GB, ST = Greater Manchester, L = Salford, O = Sectigo Limited, CN = Sectigo RSA Organization Validation Secure Server CA\n",
      "   i:C = US, ST = New Jersey, L = Jersey City, O = The USERTRUST Network, CN = USERTrust RSA Certification Authority\n",
      " 3 s:C = US, ST = New Jersey, L = Jersey City, O = The USERTRUST Network, CN = USERTrust RSA Certification Authority\n",
      "   i:C = GB, ST = Greater Manchester, L = Salford, O = Comodo CA Limited, CN = AAA Certificate Services\n",
      "---\n",
      "Server certificate\n",
      "-----BEGIN CERTIFICATE-----\n",
      "MIIG/zCCBeegAwIBAgIQKQ6LOXyup/v9Njut4flfCzANBgkqhkiG9w0BAQsFADCB\n",
      "lTELMAkGA1UEBhMCR0IxGzAZBgNVBAgTEkdyZWF0ZXIgTWFuY2hlc3RlcjEQMA4G\n",
      "A1UEBxMHU2FsZm9yZDEYMBYGA1UEChMPU2VjdGlnbyBMaW1pdGVkMT0wOwYDVQQD\n",
      "EzRTZWN0aWdvIFJTQSBPcmdhbml6YXRpb24gVmFsaWRhdGlvbiBTZWN1cmUgU2Vy\n",
      "dmVyIENBMB4XDTIyMTAxMTAwMDAwMFoXDTIzMTAxMTIzNTk1OVowfTELMAkGA1UE\n",
      "BhMCREUxFjAUBgNVBAgTDU5pZWRlcnNhY2hzZW4xPTA7BgNVBAoTNEphZGUgSG9j\n",
      "aHNjaHVsZSBXaWxoZWxtc2hhdmVuIC8gT2xkZW5idXJnIC8gRWxzZmxldGgxFzAV\n",
      "BgNVBAMTDnd3dy5qYWRlLWhzLmRlMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIB\n",
      "CgKCAQEA0efprkaeBL4+dSek2Q8txVswT7S6GvXfbPexROgiEtJSxYTLkqIcsGZl\n",
      "e4TTiV50EOhOrJxth/xj2IpH0NYut/KnkF8HavmZkHRYQFZolyfXOmFnxAIH6kCq\n",
      "XRzXaKviNCamspSZDjNHESMqw7lzifmnb5RtcEXPeYnUVIl4rdcGzr0S2jrRRVmK\n",
      "SOkyIm+Rtu0b1ET0pxi841XBuTUkKxDHru9EJa2W6tDxdXxAS5qqjf79trv0YoP+\n",
      "tA/HdVt2QMHzEjdav49OOLQpPLkVMT20+X6uqHrL5v0hX3NoUr9iYxHXnhbzOU3R\n",
      "W1+cDoHsSOjX2JnQ4tPGjUG8BzlegwIDAQABo4IDYDCCA1wwHwYDVR0jBBgwFoAU\n",
      "F9nWJSdn+THCSUPZMDZEjGypT+swHQYDVR0OBBYEFN1s4kzVWXXuW1O6h3bvUw3U\n",
      "BMx1MA4GA1UdDwEB/wQEAwIFoDAMBgNVHRMBAf8EAjAAMB0GA1UdJQQWMBQGCCsG\n",
      "AQUFBwMBBggrBgEFBQcDAjBKBgNVHSAEQzBBMDUGDCsGAQQBsjEBAgEDBDAlMCMG\n",
      "CCsGAQUFBwIBFhdodHRwczovL3NlY3RpZ28uY29tL0NQUzAIBgZngQwBAgIwWgYD\n",
      "VR0fBFMwUTBPoE2gS4ZJaHR0cDovL2NybC5zZWN0aWdvLmNvbS9TZWN0aWdvUlNB\n",
      "T3JnYW5pemF0aW9uVmFsaWRhdGlvblNlY3VyZVNlcnZlckNBLmNybDCBigYIKwYB\n",
      "BQUHAQEEfjB8MFUGCCsGAQUFBzAChklodHRwOi8vY3J0LnNlY3RpZ28uY29tL1Nl\n",
      "Y3RpZ29SU0FPcmdhbml6YXRpb25WYWxpZGF0aW9uU2VjdXJlU2VydmVyQ0EuY3J0\n",
      "MCMGCCsGAQUFBzABhhdodHRwOi8vb2NzcC5zZWN0aWdvLmNvbTCCAX8GCisGAQQB\n",
      "1nkCBAIEggFvBIIBawFpAHYArfe++nz/EMiLnT2cHj4YarRnKV3PsQwkyoWGNOvc\n",
      "gooAAAGDxsJbpQAABAMARzBFAiEA8hfUpcXUaZpILZlLp6Jvs+bc8a0+9UPR5Wpm\n",
      "6irrJLQCIBTDSenyRGmOXRlpoLSvksSDKzLHXYdolst3WlVEiljgAHcAejKMVNi3\n",
      "LbYg6jjgUh7phBZwMhOFTTvSK8E6V6NS61IAAAGDxsJbawAABAMASDBGAiEAq98L\n",
      "kHTRCSAB09qIhnk7Xtw7YMwuRoUugt5BCYW7SvcCIQCwo6eFL8f86lIF7Zc9/XF+\n",
      "iC8C/qTsqJdxllJYxl5tiAB2AOg+0No+9QY1MudXKLyJa8kD08vREWvs62nhd31t\n",
      "Br1uAAABg8bCW0YAAAQDAEcwRQIhAP8eCaqt/JeO609MJ5pLxdV3hI+DRBa5Vqli\n",
      "Rqpb/E2TAiByU7yh5ofc2g/xQ8heYvgpuxErGIj1PNB1yohKTEhQQjAlBgNVHREE\n",
      "HjAcgg53d3cuamFkZS1ocy5kZYIKamFkZS1ocy5kZTANBgkqhkiG9w0BAQsFAAOC\n",
      "AQEAL/alp/zTIVbgZ9W6YnqSQLWDYgFi9q5htD6/4cHpFqaXhw8kR7sP/q0lL5Pz\n",
      "Q7YHnwEXLhE7DyKdE+g9xgYOAJYOPbI1fCIMVAmyGazIvn2Us3w7HnlMvRheM4/m\n",
      "L2njNdJek6yeB0HhwYyxWUkJN9M4+IgBqvFmqFXB+xwN/hfwEb5yxJjp4xI8mMsX\n",
      "uBszzFzviNDUxgg8UcZZFa4714tYfw8ZJMwuLDY+4MqqoJ9yBKThOM8SnoatwqpF\n",
      "cK9IRTvhlj9OSpzUqvg6ZiJbbWiOyUeayH4xDhZ1IOu9iATh2Yz8+6YFC6X0wOJO\n",
      "eUEzA3T9dG0Gwu7jB95zT2GNUA==\n",
      "-----END CERTIFICATE-----\n",
      "subject=C = DE, ST = Niedersachsen, O = Jade Hochschule Wilhelmshaven / Oldenburg / Elsfleth, CN = www.jade-hs.de\n",
      "\n",
      "issuer=C = GB, ST = Greater Manchester, L = Salford, O = Sectigo Limited, CN = Sectigo RSA Organization Validation Secure Server CA\n",
      "\n",
      "---\n",
      "No client certificate CA names sent\n",
      "Peer signing digest: SHA256\n",
      "Peer signature type: RSA-PSS\n",
      "Server Temp Key: X25519, 253 bits\n",
      "---\n",
      "SSL handshake has read 7143 bytes and written 396 bytes\n",
      "Verification: OK\n",
      "---\n",
      "New, TLSv1.3, Cipher is TLS_AES_256_GCM_SHA384\n",
      "Server public key is 2048 bit\n",
      "Secure Renegotiation IS NOT supported\n",
      "Compression: NONE\n",
      "Expansion: NONE\n",
      "No ALPN negotiated\n",
      "Early data was not sent\n",
      "Verify return code: 0 (ok)\n",
      "---\n",
      "---\n",
      "Post-Handshake New Session Ticket arrived:\n",
      "SSL-Session:\n",
      "    Protocol  : TLSv1.3\n",
      "    Cipher    : TLS_AES_256_GCM_SHA384\n",
      "    Session-ID: 5F272A4370860008B1BBACADAE4B0A7C422EE4F05A5310DCD8FE05367BC5C712\n",
      "    Session-ID-ctx: \n",
      "    Resumption PSK: 375D5927DA30E7D39D0888946D90B38810363CB200213ABA5007FE8576B54B8DFD60E134C3B9EE47E538E744AF825A84\n",
      "    PSK identity: None\n",
      "    PSK identity hint: None\n",
      "    SRP username: None\n",
      "    TLS session ticket lifetime hint: 300 (seconds)\n",
      "    TLS session ticket:\n",
      "    0000 - 10 c6 0e db 96 8b ad ae-b9 4c 0b 85 dd 59 bd 8b   .........L...Y..\n",
      "    0010 - a5 de a2 fd e0 70 eb 55-4e c7 87 2b a4 58 ba 82   .....p.UN..+.X..\n",
      "    0020 - 71 cb 70 47 77 60 cb 6b-e5 be b7 57 dd d7 a0 5e   q.pGw`.k...W...^\n",
      "    0030 - ad 33 d4 c6 35 fa 34 f5-e7 8a 7a 9a 00 ff 51 b3   .3..5.4...z...Q.\n",
      "    0040 - 4d cc 6d 6f f2 b6 f3 e8-15 8c df 28 20 b3 c4 7c   M.mo.......( ..|\n",
      "    0050 - d1 01 05 5e ca 3e 48 8a-a3 7c 93 8d 14 1c 72 84   ...^.>H..|....r.\n",
      "    0060 - a9 7a 22 e7 9e 66 63 ea-05 0e 84 25 bc 4c 6d 0c   .z\"..fc....%.Lm.\n",
      "    0070 - 62 01 bf ba d6 95 ea 8a-0d ba 7a 31 bf 5f f8 65   b.........z1._.e\n",
      "    0080 - 5d 2f 0b 91 61 2f 33 71-fd b9 c4 53 f5 0b ce 51   ]/..a/3q...S...Q\n",
      "    0090 - 82 b3 ba 23 30 7d 7e 5e-88 41 85 5f 46 b2 8c 79   ...#0}~^.A._F..y\n",
      "    00a0 - d0 c3 de ac 32 84 a6 a9-09 66 21 25 92 bc 6c 2f   ....2....f!%..l/\n",
      "    00b0 - d0 43 ad 86 cf 5f e7 38-3c 39 f8 60 f8 1b 4b 12   .C..._.8<9.`..K.\n",
      "    00c0 - 68 a0 f8 97 44 a4 00 cb-50 db 28 28 2b 9c 43 65   h...D...P.((+.Ce\n",
      "    00d0 - 40 f6 52 fb 7e 2f d8 30-f4 a0 69 75 19 23 e4 12   @.R.~/.0..iu.#..\n",
      "    00e0 - 79 b2 87 7b ea 48 c7 30-03 f3 85 ee dc 71 6a 45   y..{.H.0.....qjE\n",
      "\n",
      "    Start Time: 1669198177\n",
      "    Timeout   : 7200 (sec)\n",
      "    Verify return code: 0 (ok)\n",
      "    Extended master secret: no\n",
      "    Max Early Data: 0\n",
      "---\n",
      "read R BLOCK\n",
      "---\n",
      "Post-Handshake New Session Ticket arrived:\n",
      "SSL-Session:\n",
      "    Protocol  : TLSv1.3\n",
      "    Cipher    : TLS_AES_256_GCM_SHA384\n",
      "    Session-ID: 796A9234299D926657C6E6AEE293FBFD3D4B76838AE90966934683F71E888037\n",
      "    Session-ID-ctx: \n",
      "    Resumption PSK: E4EF5D625C8BC5B2A382D7FC7E5ECD0ACCD41406DE6E3197CC79BD03EAD249FF8D4DB6D28EC115E258AEBD4B0854A1A8\n",
      "    PSK identity: None\n",
      "    PSK identity hint: None\n",
      "    SRP username: None\n",
      "    TLS session ticket lifetime hint: 300 (seconds)\n",
      "    TLS session ticket:\n",
      "    0000 - 10 c6 0e db 96 8b ad ae-b9 4c 0b 85 dd 59 bd 8b   .........L...Y..\n",
      "    0010 - cc 7f 2c f2 0a c1 3a b2-8a 60 19 9e 3a 0b 5c 56   ..,...:..`..:.\\V\n",
      "    0020 - a6 1a 16 89 c5 89 47 4d-e9 ca 05 d3 12 6d e8 d4   ......GM.....m..\n",
      "    0030 - 82 01 fc bf 10 b1 9b 41-41 b6 2d df 30 97 27 d1   .......AA.-.0.'.\n",
      "    0040 - 8a 86 bd b8 73 20 8d 50-3b b8 82 37 68 b7 d7 29   ....s .P;..7h..)\n",
      "    0050 - 53 2a 35 88 8d 59 2f d9-14 62 1a b7 8d df 1f ec   S*5..Y/..b......\n",
      "    0060 - d8 95 b1 b5 eb 5b af 67-c7 0d cb 44 90 5c e6 51   .....[.g...D.\\.Q\n",
      "    0070 - 30 aa cf 5d 98 42 76 19-3d 7f f4 ed e4 ca 75 d5   0..].Bv.=.....u.\n",
      "    0080 - 99 8a d1 9a a0 ef 1a f4-ac 9d 31 39 d9 52 44 43   ..........19.RDC\n",
      "    0090 - 8d 86 76 51 0e 7d 03 b3-d1 28 89 79 2f 2c 95 4b   ..vQ.}...(.y/,.K\n",
      "    00a0 - ae 26 8f 36 4f c4 82 d3-e6 af 18 46 76 4f 6b ea   .&.6O......FvOk.\n",
      "    00b0 - d7 f1 a2 c4 8e 82 44 8f-b8 61 51 7d 80 44 d0 43   ......D..aQ}.D.C\n",
      "    00c0 - a6 0e 14 f5 e9 eb 98 d3-68 92 66 9e 3a 40 70 72   ........h.f.:@pr\n",
      "    00d0 - 67 95 6d d0 72 13 57 cb-81 5b 4d b6 4b c5 72 a8   g.m.r.W..[M.K.r.\n",
      "    00e0 - f6 8a 21 22 06 ef a6 2e-10 be f7 e5 18 d4 6f 20   ..!\"..........o \n",
      "\n",
      "    Start Time: 1669198177\n",
      "    Timeout   : 7200 (sec)\n",
      "    Verify return code: 0 (ok)\n",
      "    Extended master secret: no\n",
      "    Max Early Data: 0\n",
      "---\n",
      "read R BLOCK\n",
      "HTTP/1.1 200 OK\n",
      "Date: Wed, 23 Nov 2022 10:09:37 GMT\n",
      "Server: Apache/2.4.41 (Ubuntu)\n",
      "Set-Cookie: PHPSESSID=t85dggeff09oco1g5cl4d4tuc7; path=/\n",
      "Expires: Thu, 19 Nov 1981 08:52:00 GMT\n",
      "Cache-Control: private, no-store\n",
      "Pragma: no-cache\n",
      "Content-Language: de\n",
      "X-TYPO3-Parsetime: 0ms\n",
      "Content-Type: text/html; charset=utf-8\n",
      "\n",
      "closed\n"
     ]
    }
   ],
   "source": [
    "!echo -e \"{request}\" | openssl s_client -crlf -ign_eof -connect {host}:443"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cd2eea75-ee7e-443d-a570-fe4f60e91045",
   "metadata": {},
   "source": [
    "The output from `s_client` is given in sections, divided by three dashes `---`. The first couple of lines will show the information about the server certificate:\n",
    "```\n",
    "CONNECTED(00000003)\n",
    "depth=2 C = US, ST = New Jersey, L = Jersey City, O = The USERTRUST Network, CN = USERTrust RSA Certification Authority\n",
    "verify return:1\n",
    "depth=1 C = GB, ST = Greater Manchester, L = Salford, O = Sectigo Limited, CN = Sectigo RSA Organization Validation Secure Server CA\n",
    "verify return:1\n",
    "depth=0 C = DE, ST = Niedersachsen, O = Jade Hochschule Wilhelmshaven / Oldenburg / Elsfleth, CN = www.jade-hs.de\n",
    "verify return:1\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ed7b53c9-bfd8-4dc5-b919-110ec6d230d5",
   "metadata": {},
   "source": [
    "The next section in the output lists all the certificates presented by the server in the order in which they were delivered:\n",
    "```\n",
    "Certificate chain\n",
    " 0 s:C = DE, ST = Niedersachsen, O = Jade Hochschule Wilhelmshaven / Oldenburg / Elsfleth, CN = www.jade-hs.de\n",
    "   i:C = GB, ST = Greater Manchester, L = Salford, O = Sectigo Limited, CN = Sectigo RSA Organization Validation Secure Server CA\n",
    " 1 s:C = DE, ST = Niedersachsen, O = Jade Hochschule Wilhelmshaven / Oldenburg / Elsfleth, CN = www.jade-hs.de\n",
    "   i:C = GB, ST = Greater Manchester, L = Salford, O = Sectigo Limited, CN = Sectigo RSA Organization Validation Secure Server CA\n",
    " 2 s:C = GB, ST = Greater Manchester, L = Salford, O = Sectigo Limited, CN = Sectigo RSA Organization Validation Secure Server CA\n",
    "   i:C = US, ST = New Jersey, L = Jersey City, O = The USERTRUST Network, CN = USERTrust RSA Certification Authority\n",
    " 3 s:C = US, ST = New Jersey, L = Jersey City, O = The USERTRUST Network, CN = USERTrust RSA Certification Authority\n",
    "   i:C = GB, ST = Greater Manchester, L = Salford, O = Comodo CA Limited, CN = AAA Certificate Services\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bc6e34a4-837d-4046-8b10-387cae924ec5",
   "metadata": {},
   "source": [
    "For each certificate, the first line shows the subject `s` and the second line shows the issuer information `i`.\n",
    "\n",
    "This part is very useful when you need to see exactly what certificates are sent; browser certificate viewers typically display reconstructed certificate chains that can be almost completely different from the presented ones. To determine if the chain is nominally correct, you might wish to verify that the subjects and issuers match. You start with the leaf (web server) certificate at the top, and then you go down the list, matching the issuer of the current certificate to the subject of the next. The last issuer you see can point to some root certificate that is not in the chain, or — if the self-signed root is included — it can point to itself."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d22eb54f-3b7a-4689-a261-c5eb933a0472",
   "metadata": {},
   "source": [
    "The next section in the output is the server certificate:\n",
    "```\n",
    "Server certificate\n",
    "-----BEGIN CERTIFICATE-----\n",
    "MIIG/zCCBeegAwIBAgIQKQ6LOXyup/v9Njut4flfCzANBgkqhkiG9w0BAQsFADCB\n",
    "lTELMAkGA1UEBhMCR0IxGzAZBgNVBAgTEkdyZWF0ZXIgTWFuY2hlc3RlcjEQMA4G\n",
    "[...]\n",
    "```\n",
    "followed by a section with a lot of information about the TLS connection:\n",
    "```\n",
    "No client certificate CA names sent\n",
    "Peer signing digest: SHA256\n",
    "Peer signature type: RSA-PSS\n",
    "Server Temp Key: X25519, 253 bits\n",
    "---\n",
    "SSL handshake has read 7143 bytes and written 396 bytes\n",
    "Verification: OK\n",
    "---\n",
    "New, TLSv1.3, Cipher is TLS_AES_256_GCM_SHA384\n",
    "Server public key is 2048 bit\n",
    "[...]\n",
    "```\n",
    "The most important information here is the protocol version (`TLSv1.3`) and cipher suite used (`TLS_AES_256_GCM_SHA384`). Do note that protocol information appears in two locations, which is potentially confusing when different versions are shown. The first location describes the minimum protocol requirement with the negotiated cipher suite, while the second location points to the actual protocol version currently being negotiated."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6f69c992-85b5-481d-b0f1-723ce7358756",
   "metadata": {},
   "source": [
    "And finally, after some TLSv1.3 session tickets, we see the answer of the webserver for our request:\n",
    "```\n",
    "read R BLOCK\n",
    "HTTP/1.1 200 OK\n",
    "Date: Wed, 23 Nov 2022 08:11:58 GMT\n",
    "Server: Apache/2.4.41 (Ubuntu)\n",
    "Set-Cookie: PHPSESSID=194gasqmvinerl409go0ijvap4; path=/\n",
    "Expires: Thu, 19 Nov 1981 08:52:00 GMT\n",
    "Cache-Control: private, no-store\n",
    "Pragma: no-cache\n",
    "Content-Language: de\n",
    "X-TYPO3-Parsetime: 0ms\n",
    "Content-Type: text/html; charset=utf-8\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3530dca4-ab9f-44d3-bf52-81be8f780872",
   "metadata": {},
   "source": [
    "## Exercise\n",
    "\n",
    "The `s_client` command is one way to obtain a server's certificate, but you would have to copy it from the output text. Let's say we wanted to save the certificate into a file in order to examine it more closely. There are ways to automate this as well, some of which are demonstrated in the [OpenSSL Cookbook, ch.2.5](https://www.feistyduck.com/library/openssl-cookbook/online/testing-with-openssl/extracting-remote-certificates.html).\n",
    "\n",
    "Note that OpenSSL normally uses PEM format for certificates, which is fine for us. It doesn't really matter if you store such a certificate in a file with ending `.pem` oder `.crt` or whatever, as long as you know what's inside. If you want to learn about other possible formats such as DER or P12, have a look at [openssl-format-options](https://www.openssl.org/docs/man3.0/man1/openssl-format-options.html).\n",
    "\n",
    "**Your task:** Retrieve the server certificate in PEM format and store it into a file. Perform this in a single command line (i.e., without user interaction)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "13c9bffd-e360-48aa-90e2-4ae229fdde4c",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "8d8d81aa-a6bc-470c-9be3-0f9dbee20017",
   "metadata": {},
   "source": [
    "Let's have a look at the file.\n",
    "\n",
    "**Your task:** Print the file via the `cat` shell command."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b519c67e-13bd-44c6-b442-02e54d1b9550",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "dff7bcf3-f31c-48e6-aef9-25609b6968f2",
   "metadata": {},
   "source": [
    "As the certificate is encoded in PEM format, it is not human-readable. So let's make use of the `x509` command. The [documentation](https://www.openssl.org/docs/man3.0/man1/openssl-x509.html) says:\n",
    "\n",
    "> This command is a multi-purposes certificate handling command. It can be used to print certificate information, convert certificates to various forms, edit certificate trust settings, generate certificates from scratch or from certificating requests and then self-signing them or signing them like a \"micro CA\".\n",
    "\n",
    "**Your task:** Decode the certificate. See a nice description of how to do that in the [OpenSSL Cookbook, ch.1.2.7](https://www.feistyduck.com/library/openssl-cookbook/online/openssl-command-line/examining-certificates.html)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6c7285e1-78ef-48f8-9784-fec3ae468f69",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "87b70528-bd86-4fdf-8ae9-8d6200b2e305",
   "metadata": {},
   "source": [
    "There's a lot of information in the certificate that we will cover in detail in the upcoming lecture session. For now, recall the recent lecture slides about signing and verifying a certificate and, with that in mind, have a look at the printed output again.\n",
    "\n",
    "**Your task:** Read the decoded output. Tell me:\n",
    "1. Which algorithm was used for signature generation?\n",
    "2. Where is the public key that has been signed?\n",
    "3. And where is the actual signature?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e4c4e5e0-7a89-4b6c-a7c6-fd201682d26c",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
