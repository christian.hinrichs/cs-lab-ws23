{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "a137874a-6335-4678-915b-33d66694fff1",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Part 01: Root CA\n",
    "\n",
    "Creating a new CA involves several steps: configuration, creation of a directory structure and initialization of the key files, and finally generation of the root key and certificate.\n",
    "\n",
    "### Root CA Configuration\n",
    "\n",
    "While you can configure basic OpenSSL operations directly via command line parameters, configuration files are essential for more sophisticated operations, such as root CA creation. OpenSSL configuration files are organized in *sections* and follow a dynamic referencing scheme, therefore information can be expressed in several ways.\n",
    "\n",
    "> ℹ️ **Further Information**  \n",
    "> For this tutorial, the required configuration files will be provided, but if you would like to dive deeper, then have a look at the configuration file format documentation in [config](https://www.openssl.org/docs/manmaster/man5/config.html) and [x509v3_config](https://www.openssl.org/docs/manmaster/man5/x509v3_config.html).\n",
    "\n",
    "> ℹ️ **Further Information**  \n",
    "> The configuration file details for specific subcommands are documented in their respective documentation page. For example, for the `openssl ca` command (that we will use later on), the [openssl-ca](https://www.openssl.org/docs/manmaster/man1/openssl-ca.html) page has a section *CONFIGURATION FILE OPTIONS* specifically for this command.\n",
    "\n",
    "In the provided [root-ca/root-ca.conf](root-ca/root-ca.conf) file besides this notebook, the first part (section `[default]`) contains some basic CA information, such as the name and the base URL, and the components of the CA’s distinguished name (section `[ca_dn]`). The second part directly controls the CA’s operation, such as path and time settings (section `[ca_default]`) as well as policies for the generated certificates (section `[policy_c_o_match]`). The third part contains the configuration for the `req` command (section `[req]`), which is going to be used only once, during the creation of the self-signed root certificate. The most important parts are in the extensions (section `[ca_ext]`), which indicate that the certificate is a CA (and not a client certificate). The fourth part of the configuration file (sections `[sub_ca_ext]` up to `[name_constraints]`) contains information that will be used during the construction of certificates issued by the root CA. In particular,\n",
    "\n",
    "* all certificates from this CA will be CAs as well (not client certificates) and\n",
    "* all subordinate CAs are going to be constrained, which means that the certificates they issue will be valid only for a subset of domain names and restricted uses.\n",
    "\n",
    "The fifth and final part of the configuration specifies the `[ocsp_ext]` section, i.e., the extensions to be used with the certificate for the Online Certificate Status Protocol (OCSP) response signing. OCSP is an alternative to certificate revocation lists (CRL) in order to obtain the revocation status of a certificate."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "45c86ba5-c886-4dcd-bf09-313b643c9bef",
   "metadata": {
    "tags": []
   },
   "source": [
    "### Root CA Directory Structure\n",
    "\n",
    "The first step is to create the directory structure on the target machine (in our simulated case, within the `root-ca/` directory) as specified in the configuration and initialize some of the files that will be used during the CA operation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "dca1efe2-2e15-46db-98fc-978b84905871",
   "metadata": {},
   "outputs": [],
   "source": [
    "mkdir -p root-ca/certs root-ca/db root-ca/private\n",
    "touch root-ca/db/index\n",
    "openssl rand -hex 16 > root-ca/db/serial\n",
    "echo 1001 > root-ca/db/crlnumber"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b40430a2-75bd-4be9-9a21-332247c01bfa",
   "metadata": {
    "tags": []
   },
   "source": [
    "### Root CA Key and Certificate\n",
    "\n",
    "Because a CA is actually just a certificate with certain properties, the actual provisioning of the CA consists of generating such a certificate. This is done in **four steps:**\n",
    "\n",
    "1. Generating a new private key,\n",
    "2. Calculating the according public key,\n",
    "3. Creating a *Certificate Signing Request* (CSR) for the public key,\n",
    "4. Fulfilling the CSR, i.e., signing the public key, which results in the certificate file.\n",
    "\n",
    "A CSR is a message or file that usually requests a CA to sign the provided public key that is enclosed in the CSR.\n",
    "\n",
    "You can imagine it like this: A CSR is like an envelope containing some sheets of personal information, like a name, an address and a fingerprint, as well as a freshly generated public key. Then you hand this envelope over to a CA and ask: *\"Dear CA, please sign the enclosed public key. I have provided a bunch of personal information for you in order to validate my request.\"* The CA would then check the personal information for compliance to its policies (for example, only entities within a certain address range might be allowed to apply for signing a key) and, if successful, would then sign the key and send the resulting *certificate* back to you. Also, it will keep a copy of the certificate for itself for bookkeeping reasons.\n",
    "\n",
    "In our case, we'd like to create our own private PKI, so there is no other CA to ask. Therefore we need to create a so-called \"self-signed\" certificate that does not rely on another party but can act as a trust anchor on its own.\n",
    "\n",
    "> 💬 **Advice**  \n",
    "> Steps 1., 2. and 3. can be performed in a single command using [openssl-req](https://www.openssl.org/docs/manmaster/man1/openssl-req.html) (as shown in the OpenSSL Cookbook).  \n",
    "> An alternative way is to do it separately: [openssl-genrsa](https://www.openssl.org/docs/manmaster/man1/openssl-genrsa.html) for private key, [openssl-rsa](https://www.openssl.org/docs/manmaster/man1/openssl-rsa.html) for public key and [openssl-req](https://www.openssl.org/docs/manmaster/man1/openssl-req.html) for CSR.  \n",
    "> For a quick solution, follow the Cookbook. For a deeper understanding, first read the Cookbook and then do the same procedure in separate commands. ;)\n",
    "\n",
    "> **⚠️ Attention**  \n",
    "> Normally, OpenSSL would prompt the user for some input during the process (e.g., a passphrase for encrypting or accessing a key). In Jupyter Notebook, this is not possible. You will have to pass all necessary information via command line parameters or via config file. To make it easy in this demonstration, you are allowed to disable encryption in the key generation commands (read the docs on how to do that). Further, you should pass the `-batch` option wherever applicable to disable manual confirmation of certain actions.\n",
    "\n",
    "> **⚠️ Attention**  \n",
    "> In contrast to the procedure outlined in the OpenSSL Cookbook, we won't issue the openssl commands from *within* the `root-ca/` subdirectory, but from the top-level directory. If you copy-paste commands from the cookbook, edit the path command line options *for input and output files* accordingly (i.e., add `root-ca/` as a path prefix, where applicable)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "76b8693b-cd75-4163-881a-d4c9847f319c",
   "metadata": {},
   "source": [
    "#### Steps 1.-3."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "a567a2ef-afe4-4d49-8a8e-6aa39004978a",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Generating a RSA private key\n",
      ".....................................................................................................................................................................++++\n",
      "..............................................................................................++++\n",
      "writing new private key to 'root-ca/private/root-ca.key'\n",
      "-----\n"
     ]
    }
   ],
   "source": [
    "# Following the Cookbook:\n",
    "openssl req -new -config root-ca/root-ca.conf -out root-ca/root-ca.csr -keyout root-ca/private/root-ca.key"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7b2579c0-eae1-40b2-b85e-e59eede4b06e",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Alternative approach in separate commands:\n",
    "#openssl genrsa -out root-ca/private/root-ca.key 4096\n",
    "#openssl rsa -in root-ca/private/root-ca.key -pubout -out root-ca/certs/root-ca.pub\n",
    "#openssl req -new -config root-ca/root-ca.conf -out root-ca/root-ca.csr -key root-ca/private/root-ca.key"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "20fa76bb-266a-4afe-8187-fd58ef55a7c2",
   "metadata": {},
   "source": [
    "#### Step 4"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "e884bbd9-c488-47d7-bb96-1d09258ef912",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Using configuration from root-ca/root-ca.conf\n",
      "Check that the request matches the signature\n",
      "Signature ok\n",
      "Certificate Details:\n",
      "Certificate:\n",
      "    Data:\n",
      "        Version: 3 (0x2)\n",
      "        Serial Number:\n",
      "            92:62:5b:19:99:16:57:8e:68:8a:a4:9e:a1:0c:63:07\n",
      "        Issuer:\n",
      "            countryName               = DE\n",
      "            organizationName          = MyCompany Inc\n",
      "            commonName                = Root CA\n",
      "        Validity\n",
      "            Not Before: Nov 29 08:53:46 2022 GMT\n",
      "            Not After : Nov 26 08:53:46 2032 GMT\n",
      "        Subject:\n",
      "            countryName               = DE\n",
      "            organizationName          = MyCompany Inc\n",
      "            commonName                = Root CA\n",
      "        Subject Public Key Info:\n",
      "            Public Key Algorithm: rsaEncryption\n",
      "                RSA Public-Key: (4096 bit)\n",
      "                Modulus:\n",
      "                    00:a4:0c:e4:27:67:d6:fa:d2:bb:65:2e:8a:fe:25:\n",
      "                    a6:8c:13:93:f4:44:3e:02:80:3b:bc:4c:ef:d5:d7:\n",
      "                    a1:05:42:08:46:dc:40:45:44:46:22:03:8e:81:5d:\n",
      "                    52:4f:7f:4c:c1:d4:80:6c:17:53:ed:17:ea:f7:a1:\n",
      "                    27:9e:3c:cc:b4:8b:a3:36:d0:9e:32:23:4b:4c:16:\n",
      "                    fe:ea:a9:30:b9:68:b4:fc:3e:52:4a:1e:63:56:8c:\n",
      "                    4f:1f:94:cb:56:a9:2e:27:6d:b9:3d:4b:58:59:ee:\n",
      "                    e2:f7:29:92:59:33:07:f4:a2:66:89:29:0b:1a:57:\n",
      "                    41:76:29:dc:e8:d0:f4:ff:99:9f:10:36:63:8c:ae:\n",
      "                    85:90:3e:33:af:54:2d:ba:47:26:63:b8:50:18:0b:\n",
      "                    c0:3f:7e:a6:5a:9c:c8:12:7f:25:f6:32:bd:22:f2:\n",
      "                    55:1b:2c:55:b8:ec:0d:83:f9:c8:f5:52:91:f3:ff:\n",
      "                    7b:c3:fc:5c:08:f3:58:2d:40:a9:ee:2c:b1:fa:e3:\n",
      "                    b7:ca:ff:07:ba:fb:8d:b4:e1:44:2e:6c:9c:60:e9:\n",
      "                    95:8b:1f:81:19:26:47:2e:c3:3f:46:31:5e:ef:42:\n",
      "                    ff:bf:00:4b:6d:5e:3f:ac:87:0d:a6:21:44:02:59:\n",
      "                    93:88:0d:5b:fa:3f:b5:af:1d:36:7a:93:89:ff:64:\n",
      "                    f4:17:5f:fb:ce:93:b5:de:0f:3c:3c:4d:f2:30:24:\n",
      "                    84:6d:d7:88:85:0a:34:4f:a5:69:4e:ff:68:07:0f:\n",
      "                    06:b1:fe:39:3b:34:ed:2f:16:28:3f:ea:ad:61:3d:\n",
      "                    eb:ac:d5:6c:65:0c:8b:f9:2a:e2:39:93:50:e7:31:\n",
      "                    01:18:fa:21:1d:d3:eb:b4:81:7d:cf:d8:68:da:49:\n",
      "                    1c:38:34:a8:7b:17:c6:cb:87:a8:95:1e:6a:66:04:\n",
      "                    3c:ee:ff:a5:4b:e4:36:e3:74:1e:60:a0:c9:47:a4:\n",
      "                    01:99:22:30:fe:ea:7d:d4:e6:d8:09:d6:22:46:40:\n",
      "                    05:d3:f0:f6:ac:92:e7:01:43:64:02:95:e7:e5:bf:\n",
      "                    a1:64:27:38:75:b5:aa:5e:e6:e4:e3:e7:02:1a:62:\n",
      "                    a2:6e:e4:53:ba:ad:94:74:0e:0b:14:f7:55:2b:eb:\n",
      "                    b7:75:4f:06:d1:c2:dc:eb:79:64:fc:b3:ca:89:3a:\n",
      "                    ea:7b:f1:d5:8f:d0:4c:9d:0e:3e:cd:7b:e3:72:1a:\n",
      "                    6c:2b:1d:35:b5:ab:38:7e:46:8a:03:f4:c3:c8:c3:\n",
      "                    d7:47:4a:35:e9:01:8b:44:c0:05:49:4e:35:dd:08:\n",
      "                    e4:3c:12:b6:f0:9a:0c:de:92:35:05:5b:96:73:15:\n",
      "                    2c:6e:73:9f:0c:12:12:1d:8d:ac:04:c9:d6:f1:30:\n",
      "                    80:65:cb\n",
      "                Exponent: 65537 (0x10001)\n",
      "        X509v3 extensions:\n",
      "            X509v3 Basic Constraints: critical\n",
      "                CA:TRUE\n",
      "            X509v3 Key Usage: critical\n",
      "                Certificate Sign, CRL Sign\n",
      "            X509v3 Subject Key Identifier: \n",
      "                8E:D5:E1:95:BA:DE:39:92:21:F6:77:37:07:BD:9C:DA:0B:C3:5B:F9\n",
      "Certificate is to be certified until Nov 26 08:53:46 2032 GMT (3650 days)\n",
      "\n",
      "Write out database with 1 new entries\n",
      "Data Base Updated\n"
     ]
    }
   ],
   "source": [
    "openssl ca -selfsign -config root-ca/root-ca.conf -in root-ca/root-ca.csr -out root-ca/root-ca.crt -extensions ca_ext -batch"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cca0f46c-4b69-4f44-b131-babfc0aabdd7",
   "metadata": {},
   "source": [
    "### OCSP Certificate\n",
    "\n",
    "Besides creating a certificate for itself, the root CA has to issue a certificate for its OCSP responder (we'll call it \"OCSP Root responder\" in the following). This way, clients may later securely check the revocation status of certificates from this CA at any time by calling the OCSP responder service at its URL.\n",
    "\n",
    "First, we create a key and CSR for the OCSP Root responder. The config file [root-ocsp.conf](root-ca/root-ocsp.conf) contains all necessary information for the CSR. Use that with `openssl req` to create a CSR just like above.\n",
    "\n",
    "> 💬 **Advice**  \n",
    "> For a deeper understanding, after you have successfully created the CSR, try to create it again, but without config file! You can provide all information from the config file via command line parameters. The *subject* should be something like `/C=DE/O=MyCompany Inc/CN=OCSP Root Responder`. This approach is also realized in the cookbook, so have a look there if you are unsure."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "12cdab66-7547-4b36-a8af-d1777f7da35e",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Generating a RSA private key\n",
      "...................................................+++++\n",
      ".......................................................................................+++++\n",
      "writing new private key to 'root-ca/private/root-ocsp.key'\n",
      "-----\n"
     ]
    }
   ],
   "source": [
    "openssl req -new -config root-ca/root-ocsp.conf -keyout root-ca/private/root-ocsp.key -out root-ca/root-ocsp.csr"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f856f68c-49f0-4919-8b21-7a35bef3d29d",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Without config file:\n",
    "#openssl req -new -newkey rsa:2048 -subj \"/C=DE/O=MyCompany Inc/CN=OCSP Root Responder\" -keyout root-ca/private/root-ocsp.key -out root-ca/root-ocsp.csr -nodes"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "89495277-7ac6-432f-80c5-bd85045cc7ae",
   "metadata": {},
   "source": [
    "Second, use the root CA to issue a certificate for this CSR. Have a look at [openssl-ca](https://www.openssl.org/docs/manmaster/man1/openssl-ca.html). In particular you will need to pass the `root-ca.conf` file for the `-conf` switch as well as the `root-ocsp.csr` and `root-ocsp.crt` file destinations for the `-in` and `-out` switches, respectively. The value of the `-extensions` switch has to specify `ocsp_ext`, which ensures that the config file section for OCSP signing (`[ocsp_ext]`) is loaded.\n",
    "\n",
    "> **⚠️ Attention**  \n",
    "> Don't forget to insert the appropriate `root-ca/` path prefix, where applicable.\n",
    "\n",
    "> **⚠️ Attention**  \n",
    "> Don't forget to use `-batch` mode in order to disable user interaction."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "12d247dd-4c71-4458-acff-2f9a303b82ee",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Using configuration from root-ca/root-ca.conf\n",
      "Check that the request matches the signature\n",
      "Signature ok\n",
      "Certificate Details:\n",
      "Certificate:\n",
      "    Data:\n",
      "        Version: 3 (0x2)\n",
      "        Serial Number:\n",
      "            92:62:5b:19:99:16:57:8e:68:8a:a4:9e:a1:0c:63:08\n",
      "        Issuer:\n",
      "            countryName               = DE\n",
      "            organizationName          = MyCompany Inc\n",
      "            commonName                = Root CA\n",
      "        Validity\n",
      "            Not Before: Nov 29 08:53:52 2022 GMT\n",
      "            Not After : Dec 29 08:53:52 2022 GMT\n",
      "        Subject:\n",
      "            countryName               = DE\n",
      "            organizationName          = MyCompany Inc\n",
      "            commonName                = OCSP Root Responder\n",
      "        Subject Public Key Info:\n",
      "            Public Key Algorithm: rsaEncryption\n",
      "                RSA Public-Key: (2048 bit)\n",
      "                Modulus:\n",
      "                    00:c7:5a:84:68:5f:9c:74:d4:c6:fe:a6:c3:b2:af:\n",
      "                    42:cc:c8:ab:3c:f3:36:7d:d8:a2:33:16:4e:78:4b:\n",
      "                    9d:9a:2d:4b:6f:52:10:96:bc:d7:eb:28:e4:ae:ea:\n",
      "                    9b:64:90:da:f4:6d:9f:2f:4e:09:9f:a9:e7:fb:1a:\n",
      "                    7f:9f:a2:bd:38:51:05:7e:eb:ac:10:0d:41:4d:f5:\n",
      "                    67:62:18:93:d4:f6:da:0f:33:69:5a:43:ce:34:e1:\n",
      "                    28:05:4d:ab:55:33:10:82:d6:a8:29:ab:f6:e2:64:\n",
      "                    84:d7:59:4d:fd:76:d7:0d:c2:81:3d:c3:84:2f:0d:\n",
      "                    11:b8:12:ce:b9:66:3d:26:cf:a3:4a:a6:85:e8:7c:\n",
      "                    c1:7b:5f:ed:24:75:a9:8c:51:4a:16:73:c7:42:01:\n",
      "                    80:9c:7c:23:b2:47:17:24:f8:50:7a:29:5c:cd:1c:\n",
      "                    34:b9:f5:de:14:ae:db:19:d0:1b:6c:26:34:cb:d6:\n",
      "                    89:f7:8e:7e:bb:75:92:86:1c:a6:ab:6e:6c:86:48:\n",
      "                    b2:7a:4c:fc:7e:e0:a2:71:04:6a:93:b9:78:ec:c0:\n",
      "                    e8:bd:f2:70:01:76:47:cc:cd:1a:db:d7:b6:a0:94:\n",
      "                    21:0d:71:8a:e7:e7:56:3c:1a:36:11:bf:f1:98:94:\n",
      "                    c8:57:cf:21:50:80:94:d0:df:93:f8:63:15:95:23:\n",
      "                    36:4b\n",
      "                Exponent: 65537 (0x10001)\n",
      "        X509v3 extensions:\n",
      "            X509v3 Authority Key Identifier: \n",
      "                keyid:8E:D5:E1:95:BA:DE:39:92:21:F6:77:37:07:BD:9C:DA:0B:C3:5B:F9\n",
      "\n",
      "            X509v3 Basic Constraints: critical\n",
      "                CA:FALSE\n",
      "            X509v3 Extended Key Usage: \n",
      "                OCSP Signing\n",
      "            OCSP No Check: \n",
      "\n",
      "            X509v3 Key Usage: critical\n",
      "                Digital Signature\n",
      "            X509v3 Subject Key Identifier: \n",
      "                82:E7:0B:F1:2F:06:C5:4D:A5:03:39:74:79:75:CD:66:EC:6F:83:94\n",
      "Certificate is to be certified until Dec 29 08:53:52 2022 GMT (30 days)\n",
      "\n",
      "Write out database with 1 new entries\n",
      "Data Base Updated\n"
     ]
    }
   ],
   "source": [
    "openssl ca -config root-ca/root-ca.conf -in root-ca/root-ocsp.csr -out root-ca/root-ocsp.crt -extensions ocsp_ext -days 30 -batch"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3d1c8c9c-5556-4d3e-8049-a7a8ec618bee",
   "metadata": {},
   "source": [
    "#### OCSP Testing\n",
    "\n",
    "Now we can start the OCSP Root responder. As this is a separate process, you'll have to run it from a terminal window as follows.\n",
    "\n",
    "First, let's find out where our current working directory is by executing the next cell:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "74e84b86-d30a-47f3-80f6-f04bbae4ed39",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "/home/jovyan/git/cs22/ex12-pki\n"
     ]
    }
   ],
   "source": [
    "pwd"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e26a08ec-b13f-468a-871a-2d78853654a9",
   "metadata": {},
   "source": [
    "Copy this path. Open a new terminal window here in Jupyter Notebook (File --> New --> Terminal) and navigate to this path via `cd <path>`. Then come back here."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6e724fe8-1eca-4f7e-9d16-129fc9a0b2b6",
   "metadata": {},
   "source": [
    "Is your terminal window ready? Execute the following line in that window. Afterwards, come back here.\n",
    "```bash\n",
    "openssl ocsp -port 9080 -index root-ca/db/index -rsigner root-ca/root-ocsp.crt -rkey root-ca/private/root-ocsp.key -CA root-ca/root-ca.crt -text\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4fcc567b-d8e6-4478-8208-95f23af6a9bf",
   "metadata": {},
   "source": [
    "The OCSP responder should now be awaiting connections (`ocsp: waiting for OCSP client connections...`).\n",
    "\n",
    "You can run a test by querying the revocation status of any given certificate with `openssl ocsp`.  \n",
    "(In our case, we have `root-ca.crt` and `root-ocsp.crt` available for now. If your files have different names, change them in the command, accordingly.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "a87c699a-c67e-4b32-bab5-1cdd5ebd6e63",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Response verify OK\n",
      "root-ca/root-ocsp.crt: good\n",
      "\tThis Update: Nov 29 08:54:18 2022 GMT\n",
      "root-ca/root-ca.crt: good\n",
      "\tThis Update: Nov 29 08:54:18 2022 GMT\n"
     ]
    }
   ],
   "source": [
    "openssl ocsp -issuer root-ca/root-ca.crt -CAfile root-ca/root-ca.crt -cert root-ca/root-ocsp.crt -cert root-ca/root-ca.crt -url http://localhost:9080"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ef65eb92-32e8-498a-b476-41a4850e4e81",
   "metadata": {},
   "source": [
    "In the output, verify OK means that the signatures were correctly verified, and good means that the certificate hasn’t been revoked."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "82704a95-ae83-46b3-a3f0-39d06d708991",
   "metadata": {},
   "source": [
    "## OCSP Shutdown\n",
    "\n",
    "If your are done for today, go to your open terminal windows and kill any running OCSP processes with `Ctrl-C`, so that they won't occupy system resources while you're gone."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5b4c98cc-b20d-45b7-a580-34c111bf1106",
   "metadata": {},
   "source": [
    "## End of Part 01.\n",
    "\n",
    "This concludes the *root ca* part. Proceed to **Part 02 (Subordinate CA)**."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Bash",
   "language": "bash",
   "name": "bash"
  },
  "language_info": {
   "codemirror_mode": "shell",
   "file_extension": ".sh",
   "mimetype": "text/x-sh",
   "name": "bash"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
