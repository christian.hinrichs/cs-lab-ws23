{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "fd558fec-731c-464a-a0ef-ce7c98a75b20",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Part 01: Root CA\n",
    "\n",
    "Creating a new CA involves several steps: configuration, creation of a directory structure and initialization of the key files, and finally generation of the root key and certificate.\n",
    "\n",
    "### Root CA Configuration\n",
    "\n",
    "While you can configure basic OpenSSL operations directly via command line parameters, configuration files are essential for more sophisticated operations, such as root CA creation. OpenSSL configuration files are organized in *sections* and follow a dynamic referencing scheme, therefore information can be expressed in several ways.\n",
    "\n",
    "> ℹ️ **Further Information**  \n",
    "> For this tutorial, the required configuration files will be provided, but if you would like to dive deeper, then have a look at the configuration file format documentation in [config](https://www.openssl.org/docs/manmaster/man5/config.html) and [x509v3_config](https://www.openssl.org/docs/manmaster/man5/x509v3_config.html).\n",
    "\n",
    "> ℹ️ **Further Information**  \n",
    "> The configuration file details for specific subcommands are documented in their respective documentation page. For example, for the `openssl ca` command (that we will use later on), the [openssl-ca](https://www.openssl.org/docs/manmaster/man1/openssl-ca.html) page has a section *CONFIGURATION FILE OPTIONS* specifically for this command.\n",
    "\n",
    "In the provided [root-ca/root-ca.conf](root-ca/root-ca.conf) file besides this notebook, the first part (section `[default]`) contains some basic CA information, such as the name and the base URL, and the components of the CA’s distinguished name (section `[ca_dn]`). The second part directly controls the CA’s operation, such as path and time settings (section `[ca_default]`) as well as policies for the generated certificates (section `[policy_c_o_match]`). The third part contains the configuration for the `req` command (section `[req]`), which is going to be used only once, during the creation of the self-signed root certificate. The most important parts are in the extensions (section `[ca_ext]`), which indicate that the certificate is a CA (and not a client certificate). The fourth part of the configuration file (sections `[sub_ca_ext]` up to `[name_constraints]`) contains information that will be used during the construction of certificates issued by the root CA. In particular,\n",
    "\n",
    "* all certificates from this CA will be CAs as well (not client certificates) and\n",
    "* all subordinate CAs are going to be constrained, which means that the certificates they issue will be valid only for a subset of domain names and restricted uses.\n",
    "\n",
    "The fifth and final part of the configuration specifies the `[ocsp_ext]` section, i.e., the extensions to be used with the certificate for the Online Certificate Status Protocol (OCSP) response signing. OCSP is an alternative to certificate revocation lists (CRL) in order to obtain the revocation status of a certificate."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a6a714ae-2f4d-4edc-8152-dbebbe362157",
   "metadata": {
    "tags": []
   },
   "source": [
    "### Root CA Directory Structure\n",
    "\n",
    "The first step is to create the directory structure on the target machine (in our simulated case, within the `root-ca/` directory) as specified in the configuration and initialize some of the files that will be used during the CA operation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3ac0a4e9-7a75-4cd7-80f6-3851f9b6c0e2",
   "metadata": {},
   "outputs": [],
   "source": [
    "mkdir -p root-ca/certs root-ca/db root-ca/private\n",
    "touch root-ca/db/index\n",
    "openssl rand -hex 16 > root-ca/db/serial\n",
    "echo 1001 > root-ca/db/crlnumber"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "053877b6-1b2b-4cdd-ad83-b1b0cbd8d605",
   "metadata": {
    "tags": []
   },
   "source": [
    "### Root CA Key and Certificate\n",
    "\n",
    "Because a CA is actually just a certificate with certain properties, the actual provisioning of the CA consists of generating such a certificate. This is done in **four steps:**\n",
    "\n",
    "1. Generating a new private key,\n",
    "2. Calculating the according public key,\n",
    "3. Creating a *Certificate Signing Request* (CSR) for the public key,\n",
    "4. Fulfilling the CSR, i.e., signing the public key, which results in the certificate file.\n",
    "\n",
    "A CSR is a message or file that usually requests a CA to sign the provided public key that is enclosed in the CSR.\n",
    "\n",
    "You can imagine it like this: A CSR is like an envelope containing some sheets of personal information, like a name, an address and a fingerprint, as well as a freshly generated public key. Then you hand this envelope over to a CA and ask: *\"Dear CA, please sign the enclosed public key. I have provided a bunch of personal information for you in order to validate my request.\"* The CA would then check the personal information for compliance to its policies (for example, only entities within a certain address range might be allowed to apply for signing a key) and, if successful, would then sign the key and send the resulting *certificate* back to you. Also, it will keep a copy of the certificate for itself for bookkeeping reasons.\n",
    "\n",
    "In our case, we'd like to create our own private PKI, so there is no other CA to ask. Therefore we need to create a so-called \"self-signed\" certificate that does not rely on another party but can act as a trust anchor on its own.\n",
    "\n",
    "> 💬 **Advice**  \n",
    "> Steps 1., 2. and 3. can be performed in a single command using [openssl-req](https://www.openssl.org/docs/manmaster/man1/openssl-req.html) (as shown in the OpenSSL Cookbook).  \n",
    "> An alternative way is to do it separately: [openssl-genrsa](https://www.openssl.org/docs/manmaster/man1/openssl-genrsa.html) for private key, [openssl-rsa](https://www.openssl.org/docs/manmaster/man1/openssl-rsa.html) for public key and [openssl-req](https://www.openssl.org/docs/manmaster/man1/openssl-req.html) for CSR.  \n",
    "> For a quick solution, follow the Cookbook. For a deeper understanding, first read the Cookbook and then do the same procedure in separate commands. ;)\n",
    "\n",
    "> **⚠️ Attention**  \n",
    "> Normally, OpenSSL would prompt the user for some input during the process (e.g., a passphrase for encrypting or accessing a key). In Jupyter Notebook, this is not possible. You will have to pass all necessary information via command line parameters or via config file. To make it easy in this demonstration, you are allowed to disable encryption in the key generation commands (read the docs on how to do that). Further, you should pass the `-batch` option wherever applicable to disable manual confirmation of certain actions.\n",
    "\n",
    "> **⚠️ Attention**  \n",
    "> In contrast to the procedure outlined in the OpenSSL Cookbook, we won't issue the openssl commands from *within* the `root-ca/` subdirectory, but from the top-level directory. If you copy-paste commands from the cookbook, edit the path command line options *for input and output files* accordingly (i.e., add `root-ca/` as a path prefix, where applicable)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "61edd3a7-e39d-4073-a933-2924b413c43c",
   "metadata": {},
   "source": [
    "#### Steps 1.-3."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c7614889-5a77-40ea-bed8-15a2c912c3b6",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "cfdffd9a-b32d-4a53-b53c-6aadd8d8f515",
   "metadata": {},
   "source": [
    "#### Step 4"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "320660ba-2eb4-499f-ba61-3d7bb6f8a739",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "8bd03189-7384-47c3-a7c2-b918ca72494f",
   "metadata": {},
   "source": [
    "### OCSP Certificate\n",
    "\n",
    "Besides creating a certificate for itself, the root CA has to issue a certificate for its OCSP responder (we'll call it \"OCSP Root responder\" in the following). This way, clients may later securely check the revocation status of certificates from this CA at any time by calling the OCSP responder service at its URL.\n",
    "\n",
    "First, we create a key and CSR for the OCSP Root responder. The config file [root-ocsp.conf](root-ca/root-ocsp.conf) contains all necessary information for the CSR. Use that with `openssl req` to create a CSR just like above.\n",
    "\n",
    "> 💬 **Advice**  \n",
    "> For a deeper understanding, after you have successfully created the CSR, try to create it again, but without config file! You can provide all information from the config file via command line parameters. The *subject* should be something like `/C=DE/O=MyCompany Inc/CN=OCSP Root Responder`. This approach is also realized in the cookbook, so have a look there if you are unsure."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5f8dff97-1299-4806-a872-96409915376c",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "c605a43f-a176-49f1-bd10-5c3b3a58564a",
   "metadata": {},
   "source": [
    "Second, use the root CA to issue a certificate for this CSR. Have a look at [openssl-ca](https://www.openssl.org/docs/manmaster/man1/openssl-ca.html). In particular you will need to pass the `root-ca.conf` file for the `-conf` switch as well as the `root-ocsp.csr` and `root-ocsp.crt` file destinations for the `-in` and `-out` switches, respectively. The value of the `-extensions` switch has to specify `ocsp_ext`, which ensures that the config file section for OCSP signing (`[ocsp_ext]`) is loaded.\n",
    "\n",
    "> **⚠️ Attention**  \n",
    "> Don't forget to insert the appropriate `root-ca/` path prefix, where applicable.\n",
    "\n",
    "> **⚠️ Attention**  \n",
    "> Don't forget to use `-batch` mode in order to disable user interaction."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "625f1243-7602-4000-822c-258f5ee2be45",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "93099df0-6308-4974-8417-544830e87ea8",
   "metadata": {},
   "source": [
    "#### OCSP Testing\n",
    "\n",
    "Now we can start the OCSP Root responder. As this is a separate process, you'll have to run it from a terminal window as follows.\n",
    "\n",
    "First, let's find out where our current working directory is by executing the next cell:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "af1919e2-921d-4a4f-ac25-eec0f6acc239",
   "metadata": {},
   "outputs": [],
   "source": [
    "pwd"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e4964740-87f2-4b48-9669-66779f82dd3c",
   "metadata": {},
   "source": [
    "Copy this path. Open a new terminal window here in Jupyter Notebook (File --> New --> Terminal) and navigate to this path via `cd <path>`. Then come back here."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "06449c2e-f3fb-42d2-a515-294795507bb8",
   "metadata": {},
   "source": [
    "Is your terminal window ready? Execute the following line in that window. Afterwards, come back here.\n",
    "```bash\n",
    "openssl ocsp -port 9080 -index root-ca/db/index -rsigner root-ca/root-ocsp.crt -rkey root-ca/private/root-ocsp.key -CA root-ca/root-ca.crt -text\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e1f4351e-f353-48d9-ae10-83d762b969a1",
   "metadata": {},
   "source": [
    "The OCSP responder should now be awaiting connections (`ocsp: waiting for OCSP client connections...`).\n",
    "\n",
    "You can run a test by querying the revocation status of any given certificate with `openssl ocsp`.  \n",
    "(In our case, we have `root-ca.crt` and `root-ocsp.crt` available for now. If your files have different names, change them in the command, accordingly.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "101af67c-fbcf-40d4-abf1-f0c8c4389186",
   "metadata": {},
   "outputs": [],
   "source": [
    "openssl ocsp -issuer root-ca/root-ca.crt -CAfile root-ca/root-ca.crt -cert root-ca/root-ocsp.crt -cert root-ca/root-ca.crt -url http://localhost:9080"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f5ff0b4b-3c8c-4295-8abe-aa47bf97ac9a",
   "metadata": {},
   "source": [
    "In the output, verify OK means that the signatures were correctly verified, and good means that the certificate hasn’t been revoked."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "12814b8e-4973-46c9-bd05-cb055cba35a1",
   "metadata": {},
   "source": [
    "## OCSP Shutdown\n",
    "\n",
    "If your are done for today, go to your open terminal windows and kill any running OCSP processes with `Ctrl-C`, so that they won't occupy system resources while you're gone."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "90cb6957-85a3-4d74-9be6-66297472b64f",
   "metadata": {},
   "source": [
    "## End of Part 01.\n",
    "\n",
    "This concludes the *root ca* part. Proceed to **Part 02 (Subordinate CA)**."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Bash",
   "language": "bash",
   "name": "bash"
  },
  "language_info": {
   "codemirror_mode": "shell",
   "file_extension": ".sh",
   "mimetype": "text/x-sh",
   "name": "bash"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
