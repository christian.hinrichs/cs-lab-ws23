{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "8c67ded3-11b4-4275-b173-f1f06608b736",
   "metadata": {},
   "source": [
    "### Part 04: Email Certificate\n",
    "\n",
    "As announced in the introduction, we'd also like to issue an email certificate for user `fred.flintstone@mycompanyinc.org`. Luckily, this is very similar to issuing a webserver certificate. Again, we follow these two steps:\n",
    "\n",
    "1. Create a CSR for requesting a webserver certificate,\n",
    "2. Process the CSR to create the webserver certificate,\n",
    "\n",
    "where step 1 would normally be done locally on the machine requesting the certificate (e.g. Fred's laptop), while step 2 would be executed on the CA issuing the certificate (e.g., the Sub CA). In our simulated case, we may just enter the commands using the appropriate paths in order to fulfill these steps, as already done in the previous notebook."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4e9491ea-cec4-4ecc-86e4-049eccbebccc",
   "metadata": {},
   "source": [
    "### Create CSR\n",
    "\n",
    "The config file [email.conf](email/email.conf) contains all necessary information for the CSR.\n",
    "\n",
    "> 💬 **Advice**  \n",
    "> Just as before, you may choose between several approaches to accomplish this, where a single `openssl req` with config file is the shortest, while `genrsa` + `rsa` + `req` with command line parameters is the most verbose approach."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "144a7288-9b60-4bd0-a185-d4709e9dc157",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "654f2876-7aae-4ecc-b71f-726b39b99e19",
   "metadata": {},
   "source": [
    "### Process CSR by the Sub CA\n",
    "\n",
    "Within the [sub-ca.conf](sub-ca/sub-ca.conf) file, there is a section `[client_ext]` that defines the policies for *client* certificates. From the viewpoint of the CA, when you receive a CSR for a client (e.g. email), you have to enable that section while processing the CSR. You can do that by specifying the option `-extensions client_ext` in your `openssl ca` command."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3cc93721-3fbd-49b2-b957-ce227cb7e3e7",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "3d623c15-60e0-458a-a034-4d5d36fa0320",
   "metadata": {},
   "source": [
    "### Simulate Revocation\n",
    "\n",
    "We won't simulate the signature validation again, as you already saw how this can be done. This time, I want you to try *certificate revocation*.\n",
    "\n",
    "* If not already running, start the OCSP Sub responder now. It should listen on `http://localhost:9081`.\n",
    "* Moreover, we need the `chain.pem` file again. Therefore please recreate it the same way as in the last parts and store it in the `email/` directory.\n",
    "\n",
    "> 💬 **Advice**  \n",
    "> As an alternative, you can cheat and just point the `-CAfile` parameter in the ocsp commands below to the already existing chain in `webserver/chain.pem` or `sub-ca/chain.pem`. But keep in mind that, in a real case, you would have to obtain the certificate chain in a legitimate way :)\n",
    "\n",
    "Then, as a first step, check the revocation status of our email certificate just like in the previous notebooks:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ead93cf0-e0ed-4dfe-87fc-3bd619ebc14f",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "88cf9ddd-d89b-4872-bd2f-ff019e8e2963",
   "metadata": {},
   "source": [
    "You should see `email/email.crt: good`, which means that Fred Flintstone can use his email certficate to sign his emails.\n",
    "\n",
    "Now imagine Fred Flintstone quits MyCompany Inc. The company policies dictate that any issued end-user certficates for ex-employees have to be *revoked* so that they cannot be used any more.\n",
    "\n",
    "To revoke a certificate, use the `-revoke` switch of the `ca` command; you’ll need to have a copy of the certificate you wish to revoke. Because all certificates are stored in the `certs/` directory of the issuing CA, you only need to know the serial number. If you have a \"distinguished name\", you can look for the serial number in the database."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3ca92074-97fa-493f-a19e-23f14a4bc8a1",
   "metadata": {},
   "outputs": [],
   "source": [
    "grep \"Fred Flintstone\" sub-ca/db/index"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ff71b90f-68bc-483d-9ffe-6c6cdd716c3f",
   "metadata": {},
   "source": [
    "The serial is denoted in the third column of the tabular output. We can extract it using `awk`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bbb70f62-7d98-450d-8c33-7e056bda817a",
   "metadata": {},
   "outputs": [],
   "source": [
    "CERTNAME=$(grep \"Fred Flintstone\" sub-ca/db/index | awk '{print $3}')\n",
    "echo $CERTNAME"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b9074ecb-4c86-4cfb-9da9-36f855d75149",
   "metadata": {},
   "source": [
    "Then we use this to find the respective certificate file in the `certs` directory:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "84eac7f2-0084-422f-9a6d-866d79bb3d89",
   "metadata": {},
   "outputs": [],
   "source": [
    "CERTFILE=$(find sub-ca/certs/ -name \"$CERTNAME.*\")\n",
    "echo $CERTFILE"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "da6617ad-e26d-4a73-a295-e90173b6c4e4",
   "metadata": {},
   "source": [
    "Now it's your turn again. Write the appropriate `openssl ca` command in the cell below.\n",
    "\n",
    "* For the target certificate to be revoked, you can simply enter the variable `$CERTFILE` as filename, which will be substituted by the shell with the actual file path.\n",
    "* For the value in the `-crl_reason` switch, you will have choose the correct reason. The value can be one of the following: `unspecified`, `keyCompromise`, `CACompromise`, `affiliationChanged`, `superseded`, `cessationOfOperation`, `certificateHold`, and `removeFromCRL`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "36470ad9-8024-44c5-9da2-84aaf335d7a7",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "9d291b1e-c681-4b86-acf4-9b17941cf3c4",
   "metadata": {},
   "source": [
    "The email certificate of Fred Flintstone should now be marked as *revoked* within the CAs database. We can check this by querying the OCSP again."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3659f292-64cc-4314-8a2c-0a4c317f5765",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "a88ced88-8ce8-47e1-a1f9-f848b5093fe8",
   "metadata": {},
   "source": [
    "The response should say `email/email.crt: revoked` followed by details of the revocation.  \n",
    "(If it still tells you \"good\", then try again after a minute. The OCSP Responder is only updated in certain time intervals.)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "309d0fae-52b7-4c96-a79b-1600c05f5e80",
   "metadata": {},
   "source": [
    "## Done.\n",
    "\n",
    "And that's it! Congratulations!\n",
    "\n",
    "Proceed to **Part 05 (Final Remarks, Shutdown and Cleanup)**."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Bash",
   "language": "bash",
   "name": "bash"
  },
  "language_info": {
   "codemirror_mode": "shell",
   "file_extension": ".sh",
   "mimetype": "text/x-sh",
   "name": "bash"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
