{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "bc6758da-9bac-4033-b128-089629e37043",
   "metadata": {},
   "source": [
    "## Part 03: Server Certificate\n",
    "\n",
    "With the root CA and sub CA operational, we now have a functional PKI in place and ready to issue end-user (i.e., client and server) certificates!\n",
    "\n",
    "Remember the picture in the introduction? Our company has an internal webserver for the intranet hosted on `intranet.mycompanyinc.org` that shall get a TLS certificate from our PKI. To issue such a webserver certificate, we have to follow the same two steps that you already learned in the previous notebooks:\n",
    "\n",
    "1. Create a CSR for requesting a webserver certificate\n",
    "2. Process the CSR to create the webserver certificate\n",
    "\n",
    "Again, in a real-life scenario, step 1 would be done locally on the machine requesting the certificate, while step 2 would be executed on the CA issuing the certificate. In our simulated case, we may just enter the commands using the appropriate paths in order to fulfill these steps, as already done in the previous notebook.\n",
    "\n",
    "> **⚠️ Attention**  \n",
    "> Remember: Only our Sub CA shall issue end-user certificates. You are not allowed to use our Root CA for that."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "86699077-a4ce-4850-86e0-18af53c636ff",
   "metadata": {},
   "source": [
    "### Create CSR\n",
    "\n",
    "The config file [webserver.conf](webserver/webserver.conf) contains all necessary information for the CSR.\n",
    "\n",
    "> 💬 **Advice**  \n",
    "> Just as before, you may choose between several approaches to accomplish this, where a single `openssl req` with config file is the shortest, while `genrsa` + `rsa` + `req` with command line parameters is the most verbose approach."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "970d1167-fb2c-498d-92b1-b6831c53f3a2",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "942e5f78-172b-41cc-9207-3ff187c12c84",
   "metadata": {},
   "source": [
    "### Process CSR by the Sub CA\n",
    "\n",
    "Within the [sub-ca.conf](sub-ca/sub-ca.conf) file, there is a section `[server_ext]` that defines the policies for *server* certificates. From the viewpoint of the CA, when you receive a CSR for a webserver, you have to enable that section while processing the CSR. You can do that by specifying the option `-extensions server_ext` in your `openssl ca` command."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1f4d25fe-9926-44a6-b200-6b85461e3681",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "ce9aad78-10b4-4328-90ad-0f1a7e80e678",
   "metadata": {},
   "source": [
    "### Verify\n",
    "\n",
    "Did you manage to issue the webserver certificate? Great, then let's see if the whole PKI setup actually works. For this, we simulate a client-server TLS/SSL handshake in order to verify the correctness of our setup.\n",
    "\n",
    "Assume a user, Fred, who wants to access the company's intranet at via TLS/SSL. The procedure (from the viewpoint of Fred's laptop) works as follows:\n",
    "1. The laptop connects to `intranet.mycompanyinc.org` and requests the server certificate from this host.\n",
    "2. In order to be sure that the server is authentic, the laptop *verifies* this certificate by:\n",
    "    1. validating the signature from the issuer of the certificate (*\"is it real?\"*) and\n",
    "    2. checking the revocation status of the certificate (*\"is it still valid?\"*)\n",
    "3. If the verification is successful, the laptop performs the TLS/SSL handshake and subsequently encrypts the traffic between itself and the server using the agreed parameters.\n",
    "\n",
    "We will focus on steps 2.A. and 2.B. here.  \n",
    "First (**2.A.**), we want to validate the signature of the certificate in order to answer the question *\"Is this a real certificate?\"*. Fred's laptop most likely has a *trust store* installed, which contains a bunch of trusted certificates for some commonly known root and intermediate CAs (like Sectigo or USERTrust, for example). Whenever a provided server certificate is to be verified, the *issuer* of the certificate (i.e., the CA that signed the certificate) is searched within the trust store. If it is found, the respective CA certificate is used as a trust anchor and the validity of the certificate signature is checked against this trusted CA certificate. This is done via public-key cryptography, please refer to the lecture slides for a visualization of this validation procedure.\n",
    "\n",
    "> ℹ️ **Further Information**  \n",
    "> The section *Trusted Certificate Options* of [openssl-verification-options](https://www.openssl.org/docs/manmaster/man1/openssl-verification-options.html) explains the role of *Trust Anchors* and *Trust Stores* in this regard.\n",
    "\n",
    "\n",
    "Second (**2.B.**), after being sure that the signature in the certificate is real, we still have to check whether that certificate is still valid *at the current point in time* or if it has been *revoked* by the CA for some reason. This is done by querying the revocation status of the certificate directly from the issuing CA.\n",
    "\n",
    "In our simulated case, we will set up all necessary parts of this infrastructure locally on the machine that runs this notebook."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bd192360-2751-4bf0-ae4b-9d82552bd1a4",
   "metadata": {},
   "source": [
    "#### Create the Trust Store\n",
    "\n",
    "As we operate a private PKI, our CA certificates won't be part of the preinstalled trust store. Therefore we have to create our own. This can be simply done by copying all relevant CA certificates one after another into a file in PEM format. We will call this file `chain.pem`. The following command does this for you. Change the file names if your certificate files have different names."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7bd6b862-8688-47d4-afe5-355ef2b43a66",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat sub-ca/sub-ca.crt root-ca/root-ca.crt | sed --quiet '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > webserver/chain.pem"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8bdeb8e0-4998-4631-a6bc-29af925f471d",
   "metadata": {},
   "source": [
    "#### Start Server Process\n",
    "\n",
    "The intranet server of MyCompany Inc would normally be some kind of web server like Apache or Nginx. For our current test scenario, we may simply simulate one using the `openssl s_server` command. Similarly to the previous notebooks, start a terminal window within Jupyter and navigate to the current directory. In that terminal, run the following command (change the file names if your certificate files have different names):\n",
    "\n",
    "```bash\n",
    "openssl s_server -port 4433 -cert webserver/webserver.crt -key webserver/webserver.key -CAfile root-ca/root-ca.crt -servername intranet.mycompanyinc.org -cert2 webserver/webserver.crt -key2 webserver/webserver.key\n",
    "```\n",
    "\n",
    "We use port 4433 instead of the usual 443 (HTTPS) because we aren't allowed to use the standard ports with our non-admin user. But that doesn't matter, as long as the client below uses the same port to establish the connection."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a4c43b6b-93cb-485a-ac41-65e833dc9101",
   "metadata": {},
   "source": [
    "#### 2.A. Validate the Signature\n",
    "\n",
    "While the client would normally be an employee's laptop, here we simulate one using the `openssl s_client` command that you already learned in the previous exercise. Again, start a new terminal window, navigate to the current directory an run the following as one command line:\n",
    "\n",
    "```bash\n",
    "echo | openssl s_client -connect localhost:4433 -verify_hostname intranet.mycompanyinc.org -servername intranet.mycompanyinc.org -verifyCAfile webserver/chain.pem -verify_return_error | grep -i verify\n",
    "```\n",
    "\n",
    "This should then print some lines regarding the verification of the certificates. Please note that the trailing `grep` command filters all ouput and only prints lines containing the word `verify`. If you want to see the whole output, just remove that last part from the command line.\n",
    "\n",
    "Hopefully, you'll see the following lines:\n",
    "\n",
    "```\n",
    "depth=2 C = DE, O = MyCompany Inc, CN = Root CA\n",
    "verify return:1\n",
    "depth=1 C = DE, O = MyCompany Inc, CN = Sub CA\n",
    "verify return:1\n",
    "depth=0 C = DE, O = MyCompany Inc, CN = MyCompany Inc Intranet Webserver\n",
    "verify return:1\n",
    "DONE\n",
    "Verify return code: 0 (ok)\n",
    "```\n",
    "\n",
    "The first couple of lines is the so-called pre-verification. The return status `1` indicates success.\n",
    "\n",
    "> ℹ️ **Further Information**  \n",
    "> The \"verify callback\" function is used to perform final verification of the applicability of the certificate for the particular use. When called, it is passed a `preverify_okay` field that indicates whether the certificate chain passed the basic checks that apply to all cases. A `1` means these checks passed.\n",
    ">\n",
    "> > `int verify_callback(int preverify_ok, X509_STORE_CTX *x509_ctx)`  \n",
    "> > The `verify_callback` function is used to control the behaviour when the SSL_VERIFY_PEER flag is set. It must be supplied by the application and receives two arguments: `preverify_ok` indicates, whether the verification of the certificate in question was passed (`preverify_ok=1`) or not (`preverify_ok=0`).\n",
    "> >\n",
    "> > (Source: [SSL_CTX_set_verify](https://www.openssl.org/docs/manmaster/man3/SSL_CTX_set_verify.html))\n",
    ">\n",
    "> This is what the `verify return:1` is showing.\n",
    "\n",
    "The last line then shows the result of the whole verification process. Here, the status `0 (ok)` indicates success."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "298253ff-0557-4afa-b86d-0c75b4418ebc",
   "metadata": {},
   "source": [
    "#### 2.B. Check Revocation Status\n",
    "\n",
    "For this, we first need to know the correct URI of the OCSP responder. Normally, we would extract that from the certificate. Execute the following cell and look at the output:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ab8ec7f4-c5c9-46d0-a38a-af65fd6e8e71",
   "metadata": {},
   "outputs": [],
   "source": [
    "openssl x509 -noout -ocsp_uri -in webserver/webserver.crt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "478ff2bd-08f8-4c7b-ab28-cc4705b0a86d",
   "metadata": {},
   "source": [
    "In our exercise here, however, this URI does not exist. But we can simulate it! In parts 01 and 02, you already learned how to start OCSP responder services for root CA and sub CA on the machine behind this notebook. In the current case, as the webserver certificate has been issued by the sub CA, we only need the OCSP Sub responder. If not already running, start the OCSP Sub responder now. It should then listen on `http://localhost:9081`.\n",
    "\n",
    "Then we can query the service the same way we did in the previous notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c5184be8-83fe-4d64-8fb8-df03f0b9bf37",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "69ea08fa-6e71-41f7-bef7-883d9e5570b1",
   "metadata": {},
   "source": [
    "You should see `webserver/webserver.crt: good` at the end of the output, which means that the webserver certificate has not been revoked and is valid at the current point in time."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b7b74219-685c-4a5d-afd1-6bac0a682653",
   "metadata": {},
   "source": [
    "## End of Part 03.\n",
    "\n",
    "This concludes the *webserver* part. Proceed to **Part 04 (Email Certificate)**."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Bash",
   "language": "bash",
   "name": "bash"
  },
  "language_info": {
   "codemirror_mode": "shell",
   "file_extension": ".sh",
   "mimetype": "text/x-sh",
   "name": "bash"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
