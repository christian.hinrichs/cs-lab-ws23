{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "# RSA Encryption\n",
    "\n",
    "In this exercise, you will implement\n",
    "\n",
    "* the asymmetric RSA encryption scheme\n",
    "* for textual data (strings)\n",
    "* using OAEP padding and\n",
    "* using base64 encoding."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For simplicity (and security), we may take the RSA implementation from the `cryptography` module instead of writing our own RSA code. The plain implementation of RSA is provided in the `cryptography.hazmat.primitives` module. We will use this to practice the handling of the some basic RSA components in a very simple test case (encrypting/decrypting a string)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In contrast to the previous AES exercise, we will use [type hints](https://peps.python.org/pep-0483/) instead of naming functions to indicate the expected input/output types. The following example (from the [typing documentation](https://docs.python.org/3/library/typing.html)) takes and returns a string and is annotated as follows:\n",
    "\n",
    "```python\n",
    "def greeting(name: str) -> str:\n",
    "    return 'Hello ' + name\n",
    "```\n",
    "\n",
    "Note: In this Jupyter Notebook, the type hints merely indicate the expected types so that you know what to do, there will be no automatic type checking!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Exercise\n",
    "\n",
    "Using the following code, a private/public key pair for RSA has been generated an has been saved to the `key.pem` file.\n",
    "\n",
    "```python\n",
    "from cryptography.hazmat.primitives.asymmetric import rsa\n",
    "from cryptography.hazmat.primitives import serialization\n",
    "\n",
    "def generate_new_key(pemfile='./key.pem'):\n",
    "    # Generate a key pair\n",
    "    privkey = rsa.generate_private_key(public_exponent=65537, key_size=2048)\n",
    "    # Serialize the keys\n",
    "    privpem = privkey.private_bytes(\n",
    "       encoding=serialization.Encoding.PEM,\n",
    "       format=serialization.PrivateFormat.TraditionalOpenSSL,\n",
    "       encryption_algorithm=serialization.NoEncryption()\n",
    "    )\n",
    "    # Write PEM file to disk\n",
    "    with open(pemfile, 'wb') as key_file:\n",
    "        key_file.write(privpem)\n",
    "\n",
    "generate_new_key()\n",
    "```\n",
    "\n",
    "In the *Poor man's unit testing* cell below, this file will be loaded again.\n",
    "\n",
    "*Your task:* Implement the `encrypt()` and `decrypt()` functions such that the tests in the unit testing cell pass successfully, similarly to the AES exercise. You will most likely need to read the [RSA documentation](https://cryptography.io/en/latest/hazmat/primitives/asymmetric/rsa/#) of the `cryptography` module.  And don't forget the base64 encoding/decoding."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cryptography.hazmat.primitives import hashes\n",
    "from cryptography.hazmat.primitives.asymmetric import padding\n",
    "from cryptography.hazmat.primitives.asymmetric import rsa\n",
    "from base64 import b64encode, b64decode\n",
    "\n",
    "def encrypt(plaintext: str, key: rsa.RSAPublicKey) -> str:\n",
    "    # RSA operates on bytes, not on strings\n",
    "    data_bytes = plaintext.encode()\n",
    "    # initialize the RSA cipher\n",
    "    mgf = padding.MGF1(algorithm=hashes.SHA256())\n",
    "    pad = padding.OAEP(mgf=mgf, algorithm=hashes.SHA256(), label=None)\n",
    "    # encrypt the data bytes\n",
    "    ciphertext_bytes = key.encrypt(data_bytes, pad)\n",
    "    # finally, encode the ciphertext bytes to ascii data via base64\n",
    "    ciphertext = b64encode(ciphertext_bytes).decode()\n",
    "    return ciphertext\n",
    "\n",
    "def decrypt(ciphertext: str, key: rsa.RSAPrivateKey) -> str:\n",
    "    # decode the ciphertext bytes from the given base64 ascii representation\n",
    "    ciphertext_bytes = b64decode(ciphertext)\n",
    "    # initialize the RSA cipher\n",
    "    mgf = padding.MGF1(algorithm=hashes.SHA256())\n",
    "    pad = padding.OAEP(mgf=mgf, algorithm=hashes.SHA256(), label=None)\n",
    "    # decrypt the data bytes\n",
    "    plaintext = key.decrypt(ciphertext_bytes, pad).decode()\n",
    "    return plaintext"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Poor man's unit testing"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "from cryptography.hazmat.primitives import serialization\n",
    "\n",
    "def load_keys(pemfile='./key.pem', password=None):\n",
    "    # Read serialized key data from PEM file\n",
    "    with open(pemfile, 'rb') as key_file:\n",
    "        pemdata = key_file.read()\n",
    "    # Deserialize the keys\n",
    "    privkey = serialization.load_pem_private_key(pemdata, password=password)\n",
    "    pubkey = privkey.public_key()\n",
    "    return privkey, pubkey\n",
    "\n",
    "print('Encryption/decryption test:')\n",
    "# Generate test case\n",
    "plaintext = 'security testing with python'\n",
    "print('plaintext:\\t', plaintext)\n",
    "# Load keys\n",
    "privkey, pubkey = load_keys()\n",
    "# Encrypt the data\n",
    "ciphertext = encrypt(plaintext, pubkey)\n",
    "print('ciphertext:\\t', ciphertext)\n",
    "# Test decrypt function\n",
    "decrypted = decrypt(ciphertext, privkey)\n",
    "print('decrypted:\\t', decrypted)\n",
    "assert(decrypted == plaintext)\n",
    "print('Ok')"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
