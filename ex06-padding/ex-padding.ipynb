{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Padding\n",
    "\n",
    "Block-based encryption schemes like AES operate on data blocks of a predefined *block size*.  \n",
    "Therefore they require input data length to be a multiple of the block size.  \n",
    "If the input data does not satisfiy this requirement, we have to add dummy data somehow. This is called *padding*.\n",
    "\n",
    "In this exercise, you will implement the PKCS #7 padding mechanism, see below.  \n",
    "Please implement the `pad_bytes()` function such that `do_tests()` passes successfully.\n",
    "\n",
    "### PKCS #7\n",
    "\n",
    "RFC 5652, Page 28, https://datatracker.ietf.org/doc/html/rfc5652#section-6.3\n",
    "\n",
    "> Some content-encryption algorithms assume the input length is a\n",
    "> multiple of `k` octets, where `k` is greater than one.  For such\n",
    "> algorithms, the input shall be padded at the trailing end with\n",
    "> `k-(lth mod k)` octets all having value `k-(lth mod k)`, where `lth` is\n",
    "> the length of the input.  In other words, the input is padded at\n",
    "> the trailing end with one of the following strings:\n",
    ">\n",
    ">                   01 -- if lth mod k = k-1  \n",
    ">                02 02 -- if lth mod k = k-2  \n",
    ">                    .  \n",
    ">                    .  \n",
    ">                    .  \n",
    ">          k k ... k k -- if lth mod k = 0  \n",
    ">\n",
    "> The padding can be removed unambiguously since all input is padded,\n",
    "> including input values that are already a multiple of the block size,\n",
    "> and no padding string is a suffix of another.  This padding method is\n",
    "> well defined if and only if `k` is less than 256.\n",
    "\n",
    "### Implementation hints\n",
    "\n",
    "Attention: If you take a single value from a binary string or a byte array, Python will interpret that always as `int`:\n",
    "\n",
    "```python\n",
    ">>> data = b'foo'\n",
    ">>> val = data[0]\n",
    ">>> print(f'The value {val} is of type {type(val)}')\n",
    "The value 102 is of type <class 'int'>\n",
    "```\n",
    "\n",
    "Even if the value is a non-printable character, the behaviour remains the same. In the following example, the data ends with the Byte `00010000` (which is \"16\" in decimal notation, and \"10\" in hexadecimal notation):\n",
    "\n",
    "```python\n",
    ">>> data = b'foo\\x10'\n",
    ">>> val = data[-1]\n",
    ">>> print(f'The value {val} is of type {type(val)}')\n",
    "The value 16 is of type <class 'int'>\n",
    "```\n",
    "\n",
    "If you actually need to convert a single value to a Byte object, you have to do that explicitly using `bytes([val])`, where `val` is the value that you'd like to convert:\n",
    "\n",
    "```python\n",
    ">>> binval = bytes([val])\n",
    ">>> print(f'The value {binval} is of type {type(binval)}')\n",
    "The value b'\\x10' is of type <class 'bytes'>\n",
    "```\n",
    "\n",
    "Please note the extra square brackets in the call to the bytes function! This is required to tell the function that we want to create a bytes object **containing** the given value. If we omit the square brackets, the function would interpret the given value as a length parameter and would create a bytes object of that length filled with zeros."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "def pad_bytes(data_to_pad, block_size):\n",
    "    pass\n",
    "\n",
    "def unpad_bytes(data, k):\n",
    "    # as a precaution, convert input data to bytes\n",
    "    data = bytes(data)\n",
    "    # check data size\n",
    "    lth = len(data)\n",
    "    if lth == 0 or lth % k != 0:\n",
    "        # data is empty or not padded\n",
    "        return data\n",
    "    # derive padding\n",
    "    padding_length = int(data[-1])\n",
    "    padding_value = bytes([padding_length])\n",
    "    # split data into unpadded data and padding\n",
    "    unpadded_data = data[:-padding_length]\n",
    "    padding = data[-padding_length:]\n",
    "    # check if padding has expected value\n",
    "    expected_padding = padding_length * padding_value\n",
    "    if padding != expected_padding:\n",
    "        # incorrect padding, data seems to be not padded\n",
    "        return data\n",
    "    # return data without padding\n",
    "    return unpadded_data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Testing"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# Simple test\n",
    "k = 8\n",
    "data = b'foobar'\n",
    "padded_data = pad_bytes(data, k)\n",
    "unpadded_data = unpad_bytes(padded_data, k)\n",
    "print(data)\n",
    "print(padded_data)\n",
    "print(unpadded_data)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# Systematic tests\n",
    "k = 16\n",
    "separator_line = '-' * 80\n",
    "print('Padding tests:')\n",
    "# Generate test cases\n",
    "for data_len, pad_len in [(0, 16), (1, 15), (2, 14), (15, 1), (16, 16)]:\n",
    "    print(separator_line)\n",
    "    # Build current test case\n",
    "    print('data_len = %d, pad_len = %d' % (data_len, pad_len))\n",
    "    data = b'M' * data_len\n",
    "    pad_val = bytes([pad_len])\n",
    "    padding = pad_val * pad_len\n",
    "    print('data:\\t\\t', data)\n",
    "    print('padding:\\t', padding)\n",
    "    padded = pad_bytes(data, k)\n",
    "    unpadded = unpad_bytes(data + padding, k)\n",
    "    notpadded = unpad_bytes(data, k)\n",
    "    print('padded:\\t\\t', padded)\n",
    "    print('unpadded:\\t', unpadded)\n",
    "    print('notpadded:\\t', notpadded)\n",
    "    # Test padding function\n",
    "    assert(padded == data + padding)\n",
    "    # Test unpadding function for padded data\n",
    "    assert(unpadded == data)\n",
    "    # Test unpadding function for unpadded data\n",
    "    assert(notpadded == data)\n",
    "    print('Ok')"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
